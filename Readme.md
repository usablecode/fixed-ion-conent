# Sticky content within ion-content

This directive gives you an option to wrap any content within itself and have it sticky on top.